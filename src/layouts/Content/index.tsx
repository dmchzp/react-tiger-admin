import { Outlet } from 'react-router-dom'
import { Layout } from 'antd'
import Source from './source'

const { Content } = Layout

const AppContent = () => {
  return (
    <Content className="app-content items-start">
      <Outlet />
      <Source />
    </Content>
  )
}

export default AppContent
