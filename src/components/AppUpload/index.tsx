import React, { FC } from 'react'
import { Button, message, Upload } from 'antd'

interface Iprops {
  title?: any
  accept?: string
  config?: object
  onSuccess?: any,
  onFail?: any,
  onRemove?: any,
}
const AppUpload: FC<Iprops> = ({ title = '上传', accept = '.jpg, .jpeg, .png', config, onSuccess, onFail, onRemove }) => {
  const photoProps = {
    name: 'file',
    accept,
    onChange(info: any) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList)
      }

      if (info.file.status !== 'removed') {
        onRemove && onRemove({ url: '' })
      }

      if (info.file.status === 'done') {
        message.success(`${info.file.name} 上传成功`)
        onSuccess && onSuccess({ url: info.file.response.data })
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} 上传失败`)
        onFail && onFail()
      }
    },
  }
  Object.assign(photoProps, config)

  return (
    <Upload {...photoProps}>
      <Button type="primary">{title}</Button>
    </Upload>
  )
}

export default AppUpload
