import React from 'react'
import { Card, Button, Result, Typography } from 'antd'
import { CloseCircleOutlined } from '@ant-design/icons'
import View from '@/components/View'

const { Paragraph, Text } = Typography

const StandardPages: React.FC = () => {
  return (
    <View>
      <Card title="操作失败">
        <Result
          status="error"
          title="操作失败"
          subTitle="请在重新提交之前检查并修改以下信息."
          extra={[
            <Button type="primary" key="console">
              Go Console
            </Button>,
            <Button key="buy">Buy Again</Button>,
          ]}
        >
          <div className="desc">
            <Paragraph>
              <Text
                strong
                style={{
                  fontSize: 16,
                }}
              >
                您提交的内容有以下错误:
              </Text>
            </Paragraph>
            <Paragraph>
              <CloseCircleOutlined className="site-result-demo-error-icon" /> 您的账户被冻结了. <a>Thaw immediately &gt;</a>
            </Paragraph>
            <Paragraph>
              <CloseCircleOutlined className="site-result-demo-error-icon" /> 您的帐户还没有资格申请. <a>Apply Unlock &gt;</a>
            </Paragraph>
          </div>
        </Result>
      </Card>
    </View>
  )
}
export default StandardPages
