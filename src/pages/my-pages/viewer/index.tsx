import React, { FC, useState } from 'react'
import { Card, Col, Row, Image } from 'antd'
import Viewer from 'react-viewer'
import data from './data.json'

const ViewPage: FC = () => {
  const [visible, setVisible] = useState(false)
  const images = data.map((img) => ({ src: img, alt: '' }))
  const [activeIndex, setactiveIndex] = useState(0)

  const handlePreview = (index: number) => {
    setVisible(true)
    setactiveIndex(index)
  }

  return (
    <div className="app-page-viewer">
      <Card type="inner" className="mb-5" title="Ant默认预览">
        <Row gutter={[24, 24]}>
          {images.map((image, index) => {
            return (
              <Col key={index} span={4}>
                <Image src={image.src} />
              </Col>
            )
          })}
        </Row>
      </Card>
      <Card type="inner" title="Viewer预览">
        <Row gutter={[24, 24]}>
          {images.map((image, index) => {
            return (
              <Col key={index} span={4}>
                <Image className="cursor-pointer" src={image.src} preview={false} onClick={() => { handlePreview(index) }} />
              </Col>
            )
          })}
        </Row>
      </Card>
      <Viewer visible={visible} onMaskClick={() => setVisible(false)} activeIndex={activeIndex} onClose={() => { setVisible(false) }} images={images} />
    </div>
  )
}

export default ViewPage
