import { lazy } from 'react'
import { DashboardOutlined, FileOutlined, FileWordOutlined, GlobalOutlined } from '@ant-design/icons'
import { Chat } from '@carbon/icons-react'
import lazyLoad from '@/router/utils/lazyLoad'
import { IRoute } from '@/typings/router'

const Basic: Array<IRoute> = [{
  path: '/basic',
  key: '/basic',
  label: '基础',
  name: 'basic',
  icon: <GlobalOutlined />,
  element: lazyLoad(lazy(() => import('@/layouts/index'))),
  redirect: '/basic/dashboard',
  children: [
    {
      path: '/basic/dashboard',
      key: '/basic/dashboard',
      label: '公告板',
      title: '公告板',
      name: 'dashboard',
      icon: <DashboardOutlined />,
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "dashboard" */ '@/pages/basic/dashboard'))),
    },
    {
      path: '/basic/doc',
      key: '/basic/doc',
      label: '文档',
      title: '文档',
      icon: <FileWordOutlined />,
      name: 'doc',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "doc" */ '@/pages/basic/doc'))),
    },
    {
      path: '/basic/qa',
      key: '/basic/qa',
      label: '反馈页面',
      title: '反馈页面',
      icon: <Chat />,
      name: 'qa',
      children: [
        {
          path: '/basic/qa/success',
          key: '/basic/qa/success',
          label: '操作成功',
          title: '操作成功',
          icon: <FileOutlined />,
          name: 'success',
          element: lazyLoad(lazy(() => import(/* webpackChunkName: "success" */ '@/pages/basic/qa/success'))),
        },
        {
          path: '/basic/qa/error',
          key: '/basic/qa/error',
          label: '操作失败',
          title: '操作失败',
          icon: <FileOutlined />,
          name: 'error',
          element: lazyLoad(lazy(() => import(/* webpackChunkName: "error" */ '@/pages/basic/qa/error'))),
        },
      ],
    },
    {
      path: '/basic/error',
      key: '/basic/error',
      label: '错误页面',
      title: '错误页面',
      icon: <Chat />,
      name: 'error',
      children: [
        {
          path: '/basic/error/404',
          key: '/basic/error/404',
          label: '404',
          title: '404',
          icon: <FileOutlined />,
          name: '404',
          element: lazyLoad(lazy(() => import(/* webpackChunkName: "404" */ '@/pages/basic/error/404'))),
        },
        {
          path: '/basic/error/403',
          key: '/basic/error/403',
          label: '403',
          title: '403',
          icon: <FileOutlined />,
          name: '403',
          element: lazyLoad(lazy(() => import(/* webpackChunkName: "403" */ '@/pages/basic/error/403'))),
        },
      ],
    },
  ],
}]

export default Basic
